# K8s Manager

### Build & Run
To build & run project

``` gradle build ```

``` java -jar build/libs/k8s-manager-0.0.1-SNAPSHOT.jar ```

### Testing
You can use swagger ui to test project.

* [swagger ui](http://localhost:8080/swagger-ui.html)

Default users and one context will be created by KubernetesManagerDataInitializer.java.

user1 -> email: admin@admin.com password: 123456

user2 -> email: admin2@admin.com password: HASHED

### Permitted addresses
You allowed to access endpoints which are accepting GET requests. For other endpoints like POST, PUT, PATCH and DELETE you have to pass 'Authorization' header. You can use login endpoint to get jwt token.   

* [login url](http://localhost:8080/swagger-ui.html#/authentication-controller/createAuthenticationTokenUsingPOST)

### SampleRequests

list contexts
```
    curl -X GET "http://localhost:8080/contexts" -H "accept: */*"
```

list deployments
```
    curl -X GET "http://localhost:8080/contexts/minikube/deployments" -H "accept: */*"
```

create deployment
```
    curl -X POST "http://localhost:8080/contexts/minikube/deployments" -H "accept: */*" 
         -H "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1NjY5ODQ3MDcsImV4cCI6MTU2NzAwMjcwN30.7EDWBNWQHMh0kB_3k9j3EKzryReQnTvelOh00II9EpI" 
         -H "Content-Type: application/json" 
         -d "{ \"image\": \"nginx:1.15.4\", \"name\": \"nginx-local\"}"
```

update deployment
```
    curl -X PUT "http://localhost:8080/contexts/minikube/deployments/1" -H "accept: */*" 
         -H "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1NjY5ODQ3MDcsImV4cCI6MTU2NzAwMjcwN30.7EDWBNWQHMh0kB_3k9j3EKzryReQnTvelOh00II9EpI" 
         -H "Content-Type: application/json" 
         -d "{ \"image\": \"nginx:1.15.4\", \"name\": \"nginx-local\"}"
```

update deployment image
```
    curl -X PATCH "http://localhost:8080/contexts/minikube/deployments/1" -H "accept: */*" 
         -H "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1NjY5ODQ3MDcsImV4cCI6MTU2NzAwMjcwN30.7EDWBNWQHMh0kB_3k9j3EKzryReQnTvelOh00II9EpI" 
         -H "Content-Type: application/json" 
         -d "{ \"image\": \"nginx:1.15.4\"}"
```

### Test coverage
95% classes, 81% lines covered


| package  |  class |  method |  line |
|---|---|---|---|                             
| annotation|	100% (0/0)|	100% (0/0)|	100% (0/0)    |
| config|	100% (2/2)|	100% (8/8)|	100% (29/29)      |
| controller|	100% (3/3)|	100% (13/13)|	100% (33/33)       |
| converter|	100% (2/2)|	100% (2/2)|	100% (13/13) |
| domain|	100% (6/6)|	72% (40/55)|	75% (85/112)|
| exception	|100% (5/5)	|100% (6/6)	|100% (12/12)   |
| initializer|	100% (1/1)|	100% (3/3)|	100% (27/27)|
|model	|84% (11/13)	|81% (61/75)|	84% (111/132)|
|repository	|100% (0/0)|	100% (0/0)|	100% (0/0)|
|security	|100% (8/8)|	95% (20/21)	|95% (83/87)|
|service|	100% (8/8)|	72% (18/25)	|57% (68/119)|
|K8sManagerApplication | 100% (1/1) | 0% (0/1) | 33% (1/3)|
