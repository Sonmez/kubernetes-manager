package com.utku.k8smanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;
import com.utku.k8smanager.model.dto.ContextDTO;
import com.utku.k8smanager.service.ContextService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ContextController.class, secure = false)
@RunWith(SpringRunner.class)
public class ContextControllerTest {

    @MockBean
    private ContextService contextService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void it_should_return_all_contexts() throws Exception {
        ContextDTO ctx1 = new ContextDTO();
        ctx1.setName("context-name");
        ctx1.setCluster("127.0.0.1");
        ctx1.setClusterConnectionType(ClusterConnectionType.USER_PASSWORD);

        when(contextService.findAll()).thenReturn(Arrays.asList(ctx1));

        this.mockMvc
                .perform(get("/contexts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.contextResourceList[0].context.name").value("context-name"))
                .andExpect(jsonPath("$._embedded.contextResourceList[0].context.cluster").value("127.0.0.1"))
                .andExpect(jsonPath("$._embedded.contextResourceList[0].context.clusterConnectionType").value("USER_PASSWORD"))
                .andExpect(jsonPath("$._embedded.contextResourceList[0]._links.contexts.href").value("http://localhost/contexts"))
                .andExpect(jsonPath("$._embedded.contextResourceList[0]._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._embedded.contextResourceList[0]._links.self.href").value("http://localhost/contexts/context-name"));

        verify(contextService, times(1)).findAll();
    }

    @Test
    public void it_should_return_single_context() throws Exception {
        ContextDTO ctx1 = new ContextDTO();
        ctx1.setName("context-name");
        ctx1.setCluster("127.0.0.1");
        ctx1.setClusterConnectionType(ClusterConnectionType.USER_PASSWORD);

        when(contextService.findByName("context-name")).thenReturn(ctx1);

        this.mockMvc
                .perform(get("/contexts/context-name"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.context.name").value("context-name"))
                .andExpect(jsonPath("$.context.cluster").value("127.0.0.1"))
                .andExpect(jsonPath("$.context.clusterConnectionType").value("USER_PASSWORD"))
                .andExpect(jsonPath("$._links.contexts.href").value("http://localhost/contexts"))
                .andExpect(jsonPath("$._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/contexts/context-name"));

        verify(contextService, times(1)).findByName("context-name");
    }
}