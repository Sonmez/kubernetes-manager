package com.utku.k8smanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;
import com.utku.k8smanager.model.dto.*;
import com.utku.k8smanager.service.DeploymentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DeploymentController.class, secure = false)
@RunWith(SpringRunner.class)
public class DeploymentControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeploymentService deploymentService;

    @Test
    public void it_should_return_all_contexts() throws Exception {
        DeploymentDTO deployment = new DeploymentDTO();
        deployment.setName("name");
        deployment.setContextName("context-name");
        deployment.setId(1L);
        deployment.setImage("image:12");

        when(deploymentService.findAll()).thenReturn(Arrays.asList(deployment));

        this.mockMvc
                .perform(get("/contexts/context-name/deployments"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0].deployment.name").value("name"))
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0].deployment.contextName").value("context-name"))
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0].deployment.id").value(1L))
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0].deployment.image").value("image:12"))
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0]._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._embedded.deploymentResourceList[0]._links.self.href").value("http://localhost/contexts/context-name/deployments/1"));

        verify(deploymentService, times(1)).findAll();
    }

    @Test
    public void it_should_return_single_context() throws Exception {
        DeploymentDTO deployment = new DeploymentDTO();
        deployment.setName("name");
        deployment.setContextName("context-name");
        deployment.setId(1L);
        deployment.setImage("image:12");

        when(deploymentService.findById(1L)).thenReturn(deployment);

        this.mockMvc
                .perform(get("/contexts/context-name/deployments/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deployment.name").value("name"))
                .andExpect(jsonPath("$.deployment.contextName").value("context-name"))
                .andExpect(jsonPath("$.deployment.id").value(1L))
                .andExpect(jsonPath("$.deployment.image").value("image:12"))
                .andExpect(jsonPath("$._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/contexts/context-name/deployments/1"));

        verify(deploymentService, times(1)).findById(1L);
    }

    @Test
    public void it_should_create_deployment() throws Exception {
        DeploymentCreateDTO createDTO = new DeploymentCreateDTO();
        createDTO.setName("name");
        createDTO.setImage("image:12");

        DeploymentDTO deployment = new DeploymentDTO();
        deployment.setName("name");
        deployment.setContextName("context-name");
        deployment.setId(1L);
        deployment.setImage("image:12");

        when(deploymentService.create(argThat(argument ->
                argument.getImage().equals("image:12") && argument.getName().equals("name") && argument.getContextName().equals("context-name")
        ))).thenReturn(deployment);

        this.mockMvc
                .perform(post("/contexts/context-name/deployments")
                        .content(objectMapper.writeValueAsString(createDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.deployment.name").value("name"))
                .andExpect(jsonPath("$.deployment.contextName").value("context-name"))
                .andExpect(jsonPath("$.deployment.id").value(1L))
                .andExpect(jsonPath("$.deployment.image").value("image:12"))
                .andExpect(jsonPath("$._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/contexts/context-name/deployments/1"));

        verify(deploymentService, times(1)).create(any(DeploymentCreateDTO.class));
    }

    @Test
    public void it_should_update_deployment() throws Exception {
        DeploymentUpdateDTO updateDTO = new DeploymentUpdateDTO();
        updateDTO.setName("name");
        updateDTO.setImage("image:12");

        DeploymentDTO deployment = new DeploymentDTO();
        deployment.setName("name");
        deployment.setContextName("context-name");
        deployment.setId(1L);
        deployment.setImage("image:12");

        when(deploymentService.update(argThat(argument ->
                argument.getImage().equals("image:12") && argument.getName().equals("name") && argument.getId().equals(1L)
        ))).thenReturn(deployment);

        this.mockMvc
                .perform(put("/contexts/context-name/deployments/1")
                        .content(objectMapper.writeValueAsString(updateDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.deployment.name").value("name"))
                .andExpect(jsonPath("$.deployment.contextName").value("context-name"))
                .andExpect(jsonPath("$.deployment.id").value(1L))
                .andExpect(jsonPath("$.deployment.image").value("image:12"))
                .andExpect(jsonPath("$._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/contexts/context-name/deployments/1"));

        verify(deploymentService, times(1)).update(any(DeploymentUpdateDTO.class));
    }

    @Test
    public void it_should_patch_deployment() throws Exception {
        DeploymentImageUpdateDTO updateDTO = new DeploymentImageUpdateDTO();
        updateDTO.setImage("image:12");

        DeploymentDTO deployment = new DeploymentDTO();
        deployment.setName("name");
        deployment.setContextName("context-name");
        deployment.setId(1L);
        deployment.setImage("image:12");

        when(deploymentService.deploymentImageUpdate(argThat(argument ->
                argument.getImage().equals("image:12") && argument.getId().equals(1L)
        ))).thenReturn(deployment);

        this.mockMvc
                .perform(patch("/contexts/context-name/deployments/1")
                        .content(objectMapper.writeValueAsString(updateDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.deployment.name").value("name"))
                .andExpect(jsonPath("$.deployment.contextName").value("context-name"))
                .andExpect(jsonPath("$.deployment.id").value(1L))
                .andExpect(jsonPath("$.deployment.image").value("image:12"))
                .andExpect(jsonPath("$._links.deployments.href").value("http://localhost/contexts/context-name/deployments"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/contexts/context-name/deployments/1"));

        verify(deploymentService, times(1)).deploymentImageUpdate(any(DeploymentImageUpdateDTO.class));
    }

    @Test
    public void it_should_delete_deployment() throws Exception {

        this.mockMvc
                .perform(delete("/contexts/context-name/deployments/1"))
                .andExpect(status().isNoContent());

        verify(deploymentService, times(1)).deleteById(1L);
    }
}