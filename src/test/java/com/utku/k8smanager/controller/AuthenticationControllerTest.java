package com.utku.k8smanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utku.k8smanager.model.request.LoginRequest;
import com.utku.k8smanager.service.AuthenticationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AuthenticationController.class, secure = false)
@RunWith(SpringRunner.class)
public class AuthenticationControllerTest {

    @MockBean
    private AuthenticationService authenticationService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void it_should_login_and_return_token() throws Exception {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("admin");
        loginRequest.setPassword("1234");

        when(authenticationService.authenticate("admin", "1234")).thenReturn("token_jwt");

        this.mockMvc
                .perform(post("/authentication/login")
                        .content(this.objectMapper.writeValueAsBytes(loginRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").value("Bearer token_jwt"));

        verify(authenticationService, times(1)).authenticate("admin", "1234");
    }

}