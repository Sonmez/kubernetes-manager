package com.utku.k8smanager.config;

import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.exception.DeploymentNotFoundException;
import com.utku.k8smanager.exception.TokenNotValidException;
import com.utku.k8smanager.model.dto.ErrorDTO;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.BadCredentialsException;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GlobalControllerExceptionHandlerTest {

    @InjectMocks
    private GlobalControllerExceptionHandler globalControllerExceptionHandler;

    @Test
    public void it_should_handle_exception() {
        // Given
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest("method", "requestURI");
        Exception exception = new Exception();


        // When
        ResponseEntity<ErrorDTO> responseEntity = globalControllerExceptionHandler.handleException(mockHttpServletRequest, exception);

        // Then
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        ErrorDTO errorDTO = responseEntity.getBody();
        assertThat(errorDTO.getTitle()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        assertThat(errorDTO.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
        assertThat(errorDTO.getDetail()).isEqualTo("Exception occurred");
        assertThat(errorDTO.getRequestUri()).isEqualTo("requestURI");
        assertThat(errorDTO.getRequestMethod()).isEqualTo("method");
        assertThat(errorDTO.getInstant()).isNotBlank();
    }

    @Test
    public void it_should_handle_ContextNotFoundException() {
        // Given
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest("method", "requestURI");
        ContextNotFoundException exception = new ContextNotFoundException();

        // When
        ResponseEntity<ErrorDTO> responseEntity = globalControllerExceptionHandler.handle(mockHttpServletRequest, exception);

        // Then
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.NOT_FOUND);

        ErrorDTO errorDTO = responseEntity.getBody();
        assertThat(errorDTO.getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
        assertThat(errorDTO.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(errorDTO.getDetail()).isEqualTo("context couldn't be found");
        assertThat(errorDTO.getRequestUri()).isEqualTo("requestURI");
        assertThat(errorDTO.getRequestMethod()).isEqualTo("method");
        assertThat(errorDTO.getInstant()).isNotBlank();
    }

    @Test
    public void it_should_handle_DeploymentNotFoundException() {
        // Given
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest("method", "requestURI");
        DeploymentNotFoundException exception = new DeploymentNotFoundException();

        // When
        ResponseEntity<ErrorDTO> responseEntity = globalControllerExceptionHandler.handle(mockHttpServletRequest, exception);

        // Then
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.NOT_FOUND);

        ErrorDTO errorDTO = responseEntity.getBody();
        assertThat(errorDTO.getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
        assertThat(errorDTO.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(errorDTO.getDetail()).isEqualTo("deployment couldn't be found");
        assertThat(errorDTO.getRequestUri()).isEqualTo("requestURI");
        assertThat(errorDTO.getRequestMethod()).isEqualTo("method");
        assertThat(errorDTO.getInstant()).isNotBlank();
    }

    @Test
    public void it_should_handle_TokenNotValidException() {
        // Given
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest("method", "requestURI");
        TokenNotValidException exception = new TokenNotValidException("message");

        // When
        ResponseEntity<ErrorDTO> responseEntity = globalControllerExceptionHandler.handle(mockHttpServletRequest, exception);

        // Then
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.FORBIDDEN);

        ErrorDTO errorDTO = responseEntity.getBody();
        assertThat(errorDTO.getTitle()).isEqualTo(HttpStatus.FORBIDDEN.getReasonPhrase());
        assertThat(errorDTO.getStatus()).isEqualTo(HttpStatus.FORBIDDEN.value());
        assertThat(errorDTO.getDetail()).isEqualTo("message");
        assertThat(errorDTO.getRequestUri()).isEqualTo("requestURI");
        assertThat(errorDTO.getRequestMethod()).isEqualTo("method");
        assertThat(errorDTO.getInstant()).isNotBlank();
    }

    @Test
    public void it_should_handle_BadCredentialsException() {
        // Given
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest("method", "requestURI");
        BadCredentialsException exception = new BadCredentialsException("message");

        // When
        ResponseEntity<ErrorDTO> responseEntity = globalControllerExceptionHandler.handle(mockHttpServletRequest, exception);

        // Then
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.FORBIDDEN);

        ErrorDTO errorDTO = responseEntity.getBody();
        assertThat(errorDTO.getTitle()).isEqualTo(HttpStatus.FORBIDDEN.getReasonPhrase());
        assertThat(errorDTO.getStatus()).isEqualTo(HttpStatus.FORBIDDEN.value());
        assertThat(errorDTO.getDetail()).isEqualTo("message");
        assertThat(errorDTO.getRequestUri()).isEqualTo("requestURI");
        assertThat(errorDTO.getRequestMethod()).isEqualTo("method");
        assertThat(errorDTO.getInstant()).isNotBlank();
    }

}