package com.utku.k8smanager.service;

import com.utku.k8smanager.converter.ContextConverter;
import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.builder.ContextBuilder;
import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.model.dto.ContextDTO;
import com.utku.k8smanager.repository.ContextRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ContextServiceTest {

    @InjectMocks
    private ContextService contextService;

    @Mock
    private ContextRepository contextRepository;

    @Mock
    private ContextConverter contextConverter;

    @Test
    public void it_should_return_all_contexts() {
        // Given
        Context ctx1 = ContextBuilder.builder().name("ctx1").build();
        Context ctx2 = ContextBuilder.builder().name("ctx2").build();

        ContextDTO dto1 = new ContextDTO();
        ContextDTO dto2 = new ContextDTO();

        when(contextRepository.findAll()).thenReturn(Arrays.asList(ctx1, ctx2));
        when(contextConverter.apply(ctx1)).thenReturn(dto1);
        when(contextConverter.apply(ctx2)).thenReturn(dto2);

        // When
        List<ContextDTO> contexts = contextService.findAll();

        // Then
        assertThat(contexts).hasSize(2).containsExactly(dto1, dto2);
    }

    @Test
    public void it_should_find_context_by_name() {
        // Given
        Context ctx = ContextBuilder.builder().name("ctx").build();

        ContextDTO dto = new ContextDTO();

        when(contextRepository.findById("ctx")).thenReturn(Optional.of(ctx));
        when(contextConverter.apply(ctx)).thenReturn(dto);

        // When
        ContextDTO context = contextService.findByName("ctx");

        // Then
        assertThat(context).isEqualTo(dto);
    }

    @Test
    public void it_should_not_throw_exception_when_context_could_not_be_found() {
        // Given

        when(contextRepository.findById("ctx")).thenReturn(Optional.empty());

        // When - Then
        try {
            contextService.findByName("ctx");
            fail();
        } catch (ContextNotFoundException e) {
            assertThat(e).isNotNull();
        }
    }
}