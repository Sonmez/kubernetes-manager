package com.utku.k8smanager.service;

import com.utku.k8smanager.converter.DeploymentConverter;
import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.K8sDeployment;
import com.utku.k8smanager.domain.builder.ContextBuilder;
import com.utku.k8smanager.domain.builder.DeploymentBuilder;
import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.exception.DeploymentAlreadyCreatedByNameException;
import com.utku.k8smanager.exception.DeploymentNotFoundException;
import com.utku.k8smanager.model.dto.DeploymentCreateDTO;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import com.utku.k8smanager.model.dto.DeploymentImageUpdateDTO;
import com.utku.k8smanager.model.dto.DeploymentUpdateDTO;
import com.utku.k8smanager.repository.ContextRepository;
import com.utku.k8smanager.repository.DeploymentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowableOfType;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeploymentServiceTest {

    @InjectMocks
    private DeploymentService deploymentService;

    @Mock
    private DeploymentRepository deploymentRepository;

    @Mock
    private DeploymentConverter deploymentConverter;

    @Mock
    private ContextRepository contextRepository;

    @Mock
    private KubernetesDeploymentService kubernetesDeploymentService;

    @Test
    public void it_should_return_all_deployments() {
        // Given
        K8sDeployment deployment1 = DeploymentBuilder.builder().id(1L).image("image1").name("name1").build();
        K8sDeployment deployment2 = DeploymentBuilder.builder().id(2L).image("image2").name("name2").build();

        DeploymentDTO deploymentDTO1 = new DeploymentDTO();
        DeploymentDTO deploymentDTO2 = new DeploymentDTO();

        when(deploymentRepository.findAll()).thenReturn(Arrays.asList(deployment1, deployment2));
        when(deploymentConverter.apply(deployment1)).thenReturn(deploymentDTO1);
        when(deploymentConverter.apply(deployment2)).thenReturn(deploymentDTO2);

        // When
        List<DeploymentDTO> deploymentDTOs = deploymentService.findAll();

        // Then
        assertThat(deploymentDTOs).hasSize(2).containsExactly(deploymentDTO1, deploymentDTO2);
    }

    @Test
    public void it_should_find_deployment_by_id() {
        // Given
        K8sDeployment deployment1 = DeploymentBuilder.builder().id(1L).image("image1").name("name1").build();

        DeploymentDTO deploymentDTO1 = new DeploymentDTO();

        when(deploymentRepository.findById(1L)).thenReturn(Optional.of(deployment1));
        when(deploymentConverter.apply(deployment1)).thenReturn(deploymentDTO1);

        // When
        DeploymentDTO actualDTO = deploymentService.findById(1L);

        // Then
        assertThat(actualDTO).isEqualTo(deploymentDTO1);
    }

    @Test
    public void it_should_throw_exception_when_deployment_can_not_Be_found() {
        // Given
        when(deploymentRepository.findById(1L)).thenReturn(Optional.empty());

        // When
        DeploymentNotFoundException deploymentNotFoundException = catchThrowableOfType(() -> deploymentService.findById(1L), DeploymentNotFoundException.class);

        // Then
        assertThat(deploymentNotFoundException).isNotNull();
    }

    @Test
    public void it_should_delete_deployment_by_id() {
        deploymentService.deleteById(1L);

        verify(deploymentRepository, times(1)).deleteById(1L);
    }

    @Test
    public void it_should_create_deployment() {
        // Given
        DeploymentCreateDTO dto = new DeploymentCreateDTO();
        dto.setImage("image");
        dto.setName("name");
        dto.setContextName("context-name");

        Context context = ContextBuilder.builder().name("context-name").build();

        when(deploymentRepository.findByName("name")).thenReturn(Optional.empty());
        K8sDeployment persistedDeployment = DeploymentBuilder.builder().id(1L).name("name").image("image").build();

        when(deploymentRepository.save(argThat(argument ->
                argument.getId() == null
                && "image".equals(argument.getImage())
                && "name".equals(argument.getName())
                && argument.getContext().equals(context)))
        ).thenReturn(persistedDeployment);

        DeploymentDTO deploymentDTO = new DeploymentDTO();
        when(deploymentConverter.apply(persistedDeployment)).thenReturn(deploymentDTO);

        when(contextRepository.findById("context-name")).thenReturn(Optional.of(context));

        // When
        DeploymentDTO actualDTO = deploymentService.create(dto);

        // Then
        assertThat(actualDTO).isNotNull().isEqualTo(deploymentDTO);

        verify(kubernetesDeploymentService).createOrReplaceDeployment(persistedDeployment);
    }

    @Test
    public void it_should_throw_exception_when_context_not_found_during_creation() {
        // Given
        DeploymentCreateDTO dto = new DeploymentCreateDTO();
        dto.setImage("image");
        dto.setName("name");
        dto.setContextName("context-name");

        when(deploymentRepository.findByName("name")).thenReturn(Optional.empty());
        when(contextRepository.findById("context-name")).thenReturn(Optional.empty());

        // When
        ContextNotFoundException contextNotFoundException = catchThrowableOfType(() -> deploymentService.create(dto), ContextNotFoundException.class);

        // Then
        assertThat(contextNotFoundException).isNotNull();
        verifyZeroInteractions(deploymentConverter);
    }

    @Test
    public void it_should_throw_exception_when_deployment_name_already_persisted_during_creation() {
        // Given
        DeploymentCreateDTO dto = new DeploymentCreateDTO();
        dto.setImage("image");
        dto.setName("name");

        K8sDeployment persistedDeployment = DeploymentBuilder.builder().id(1L).name("name").image("image").build();
        when(deploymentRepository.findByName("name")).thenReturn(Optional.of(persistedDeployment));


        // When
        DeploymentAlreadyCreatedByNameException exception = catchThrowableOfType(() -> deploymentService.create(dto), DeploymentAlreadyCreatedByNameException.class);

        // Then
        assertThat(exception).isNotNull();
        verify(deploymentRepository, never()).save(any(K8sDeployment.class));
    }

    @Test
    public void it_should_update_deployment() {
        // Given
        DeploymentUpdateDTO dto = new DeploymentUpdateDTO();
        dto.setImage("image");
        dto.setName("name");
        dto.setId(1L);

        when(deploymentRepository.findByName("name")).thenReturn(Optional.empty());
        K8sDeployment persistedDeployment = DeploymentBuilder.builder().id(1L).name("name").image("image").build();
        when(deploymentRepository.findById(1L)).thenReturn(Optional.of(persistedDeployment));

        when(deploymentRepository.save(argThat(argument ->
                argument.getId() == 1L && "image".equals(argument.getImage()) && "name".equals(argument.getName()))
        )).thenReturn(persistedDeployment);

        DeploymentDTO deploymentDTO = new DeploymentDTO();
        when(deploymentConverter.apply(persistedDeployment)).thenReturn(deploymentDTO);

        // When
        DeploymentDTO actualDTO = deploymentService.update(dto);

        // Then
        assertThat(actualDTO).isNotNull().isEqualTo(deploymentDTO);
    }

    @Test
    public void it_should_update_deployment_image() {
        // Given
        DeploymentImageUpdateDTO dto = new DeploymentImageUpdateDTO();
        dto.setImage("image100");
        dto.setId(1L);

        K8sDeployment persistedDeployment = DeploymentBuilder.builder().id(1L).name("name").image("image").build();
        when(deploymentRepository.findById(1L)).thenReturn(Optional.of(persistedDeployment));

        when(deploymentRepository.save(argThat(argument ->
                argument.getId() == 1L && "image100".equals(argument.getImage()) && "name".equals(argument.getName()))
        )).thenReturn(persistedDeployment);

        DeploymentDTO deploymentDTO = new DeploymentDTO();
        when(deploymentConverter.apply(persistedDeployment)).thenReturn(deploymentDTO);

        // When
        DeploymentDTO actualDTO = deploymentService.deploymentImageUpdate(dto);

        // Then
        assertThat(actualDTO).isNotNull().isEqualTo(deploymentDTO);
    }

    @Test
    public void it_should_throw_exception_when_deployment_name_already_persisted_during_update() {
        // Given
        DeploymentUpdateDTO dto = new DeploymentUpdateDTO();
        dto.setImage("image");
        dto.setName("name");
        dto.setId(1L);

        K8sDeployment persistedDeployment = DeploymentBuilder.builder().id(1L).name("name").image("image").build();
        when(deploymentRepository.findByName("name")).thenReturn(Optional.of(persistedDeployment));

        // When
        DeploymentAlreadyCreatedByNameException exception = catchThrowableOfType(() -> deploymentService.update(dto), DeploymentAlreadyCreatedByNameException.class);

        // Then
        assertThat(exception).isNotNull();
        verify(deploymentRepository, never()).save(any(K8sDeployment.class));
    }
}