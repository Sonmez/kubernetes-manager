package com.utku.k8smanager.converter;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.K8sDeployment;
import com.utku.k8smanager.domain.builder.ContextBuilder;
import com.utku.k8smanager.domain.builder.DeploymentBuilder;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DeploymentConverterTest {

    @InjectMocks
    private DeploymentConverter deploymentConverter;

    @Test
    public void it_should_convert_to_deployment_dto() {
        // Given
        Context context = ContextBuilder.builder().name("context-name").build();
        K8sDeployment deployment = DeploymentBuilder.builder()
                .id(1L)
                .image("image")
                .name("name")
                .context(context).build();

        // When
        DeploymentDTO dto = deploymentConverter.apply(deployment);

        // Then
        assertThat(dto.getId()).isEqualTo(1L);
        assertThat(dto.getImage()).isEqualTo("image");
        assertThat(dto.getName()).isEqualTo("name");
        assertThat(dto.getContextName()).isEqualTo("context-name");
    }
}