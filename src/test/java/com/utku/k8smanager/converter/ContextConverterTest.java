package com.utku.k8smanager.converter;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.builder.ContextBuilder;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;
import com.utku.k8smanager.model.dto.ContextDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ContextConverterTest {

    @InjectMocks
    private ContextConverter contextConverter;

    @Test
    public void it_should_convert_to_dto() {
        // Given
        Context context = ContextBuilder.builder()
                .name("context-name")
                .cluster("127.0.0.1")
                .clusterConnectionType(ClusterConnectionType.USER_PASSWORD)
                .build();

        // When
        ContextDTO contextDTO = contextConverter.apply(context);

        // Then
        assertThat(contextDTO.getClusterConnectionType()).isEqualTo(ClusterConnectionType.USER_PASSWORD);
        assertThat(contextDTO.getCluster()).isEqualTo("127.0.0.1");
        assertThat(contextDTO.getName()).isEqualTo("context-name");
    }
}