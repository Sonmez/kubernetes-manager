package com.utku.k8smanager.integrationtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utku.k8smanager.domain.User;
import com.utku.k8smanager.model.request.LoginRequest;
import com.utku.k8smanager.repository.UserRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class AuthenticationIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void before(){
        RestAssured.port = this.port;
    }

    @Test
    public void it_should_login_and_return_token() {
        User user = new User();
        user.setEmail("admin-test@admin.com");
        user.setPassword(passwordEncoder.encode("123456"));
        userRepository.save(user);
        userRepository.flush();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("admin-test@admin.com");
        loginRequest.setPassword("123456");

        String token = given()
                .contentType(ContentType.JSON)
                .body(loginRequest)

        .when()
                .post("/authentication/login")

        .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("token", notNullValue())
                .extract().path("token");

        assertThat(token).startsWith("Bearer ");
    }

    @Test
    public void it_should_not_login_and_403_status_code() throws Exception {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("admin");
        loginRequest.setPassword("12345");

        given()
                .contentType(ContentType.JSON)
                .body(loginRequest)

        .when()
                .post("/authentication/login")
        .then()
                .assertThat()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

    @Test
    public void it_should_check_token_valid_and_return_200_status_code() {
        User user = new User();
        user.setEmail("admin-test-2@admin.com");
        user.setPassword(passwordEncoder.encode("123456"));
        userRepository.save(user);
        userRepository.flush();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("admin-test-2@admin.com");
        loginRequest.setPassword("123456");

        String token = given().contentType(ContentType.JSON)
                .body(loginRequest)
        .when()
                .post("/authentication/login")
        .then()
                .extract().path("token");

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", token)
        .when()
                .post("/authentication/valid")
        .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void it_should_check_token_valid_and_return_403_status_code() {

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", "not-valid-token")
        .when()
                .post("/authentication/valid")
        .then()
                .assertThat()
                .statusCode(HttpStatus.FORBIDDEN.value());
    }

}
