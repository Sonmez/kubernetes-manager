package com.utku.k8smanager.domain.enums;

public enum ClusterConnectionType {

    CERTIFICATES,
    USER_PASSWORD;

}
