package com.utku.k8smanager.domain;


import com.utku.k8smanager.domain.enums.ClusterConnectionType;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CONTEXT")
public class Context {

    @Id
    @Column(name = "NAME")
    private String name;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "CLUSTER", nullable = false)
    private String cluster;

    @Column(name = "CERT_DATA", length = 2000)
    private String certData;

    @Column(name = "CLIENT_KEY", length = 2000)
    private String clientKey;

    @Enumerated(EnumType.STRING)
    private ClusterConnectionType clusterConnectionType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCertData() {
        return certData;
    }

    public void setCertData(String certData) {
        this.certData = certData;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public ClusterConnectionType getClusterConnectionType() {
        return clusterConnectionType;
    }

    public void setClusterConnectionType(ClusterConnectionType clusterConnectionType) {
        this.clusterConnectionType = clusterConnectionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Context context = (Context) o;
        return Objects.equals(name, context.name) &&
                Objects.equals(cluster, context.cluster);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cluster);
    }
}