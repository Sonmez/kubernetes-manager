package com.utku.k8smanager.domain.builder;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.K8sDeployment;

public final class DeploymentBuilder {
    private Long id;
    private String name;
    private String image;
    private Context context;

    private DeploymentBuilder() {
    }

    public static DeploymentBuilder builder() {
        return new DeploymentBuilder();
    }

    public DeploymentBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public DeploymentBuilder name(String name) {
        this.name = name;
        return this;
    }

    public DeploymentBuilder image(String image) {
        this.image = image;
        return this;
    }

    public DeploymentBuilder context(Context context) {
        this.context = context;
        return this;
    }

    public K8sDeployment build() {
        K8sDeployment deployment = new K8sDeployment();
        deployment.setId(id);
        deployment.setName(name);
        deployment.setImage(image);
        deployment.setContext(context);
        return deployment;
    }
}
