package com.utku.k8smanager.domain.builder;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;

public final class ContextBuilder {
    private String name;
    private String username;
    private String password;
    private String cluster;
    private String certData;
    private String clientKey;
    private ClusterConnectionType clusterConnectionType;

    private ContextBuilder() {
    }

    public static ContextBuilder builder() {
        return new ContextBuilder();
    }

    public ContextBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ContextBuilder username(String username) {
        this.username = username;
        return this;
    }

    public ContextBuilder password(String password) {
        this.password = password;
        return this;
    }

    public ContextBuilder cluster(String cluster) {
        this.cluster = cluster;
        return this;
    }

    public ContextBuilder certData(String certData) {
        this.certData = certData;
        return this;
    }

    public ContextBuilder clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }

    public ContextBuilder clusterConnectionType(ClusterConnectionType clusterConnectionType) {
        this.clusterConnectionType = clusterConnectionType;
        return this;
    }

    public Context build() {
        Context context = new Context();
        context.setName(name);
        context.setUsername(username);
        context.setPassword(password);
        context.setCluster(cluster);
        context.setCertData(certData);
        context.setClientKey(clientKey);
        context.setClusterConnectionType(clusterConnectionType);
        return context;
    }
}
