package com.utku.k8smanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8sManagerApplication.class, args);
	}

}
