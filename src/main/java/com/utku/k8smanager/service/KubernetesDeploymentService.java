package com.utku.k8smanager.service;

import com.utku.k8smanager.domain.K8sDeployment;
import com.utku.k8smanager.service.configfactory.KubernetesConfigFactory;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.apps.DeploymentSpecBuilder;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class KubernetesDeploymentService {

    private static final String DEFAULT_NAMESPACE = "default";

    private final KubernetesConfigFactory configFactory;

    public KubernetesDeploymentService(KubernetesConfigFactory configFactory) {
        this.configFactory = configFactory;
    }

    public void createOrReplaceDeployment(K8sDeployment deployment) {
        Config config = configFactory.buildFor(deployment.getContext());
        KubernetesClient client = new DefaultKubernetesClient(config);

        Map<String, String> labels = new HashMap<>();
        labels.put("app", deployment.getName());

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(deployment.getName());
        metadata.setLabels(labels);

        ContainerPort containerPort = new ContainerPort();
        containerPort.setContainerPort(80);

        Container container = new ContainerBuilder()
                .withName(deployment.getName())
                .withImage(deployment.getImage())
                .withPorts(containerPort)
                .build();

        PodSpec spec = new PodSpec();
        spec.setContainers(Collections.singletonList(container));

        client.apps().deployments()
                .inNamespace(DEFAULT_NAMESPACE)
                .createOrReplaceWithNew()
                .withApiVersion("apps/v1")
                .withKind("Deployment")
                .withMetadata(metadata)
                .withSpec(new DeploymentSpecBuilder()
                        .withReplicas(2)
                        .withSelector(new LabelSelectorBuilder().withMatchLabels(labels).build())
                        .withTemplate(new PodTemplateSpecBuilder()
                                .withMetadata(metadata)
                                .withSpec(spec)
                                .build())
                        .build())
                .done();
    }



}
