package com.utku.k8smanager.service;

import com.utku.k8smanager.converter.DeploymentConverter;
import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.K8sDeployment;
import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.exception.DeploymentAlreadyCreatedByNameException;
import com.utku.k8smanager.exception.DeploymentNotFoundException;
import com.utku.k8smanager.model.dto.DeploymentCreateDTO;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import com.utku.k8smanager.model.dto.DeploymentImageUpdateDTO;
import com.utku.k8smanager.model.dto.DeploymentUpdateDTO;
import com.utku.k8smanager.repository.ContextRepository;
import com.utku.k8smanager.repository.DeploymentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DeploymentService {

    private final DeploymentRepository deploymentRepository;
    private final DeploymentConverter deploymentConverter;
    private final ContextRepository contextRepository;
    private final KubernetesDeploymentService kubernetesDeploymentService;

    public DeploymentService(DeploymentRepository deploymentRepository, DeploymentConverter deploymentConverter, ContextRepository contextRepository, KubernetesDeploymentService kubernetesDeploymentService) {
        this.deploymentRepository = deploymentRepository;
        this.deploymentConverter = deploymentConverter;
        this.contextRepository = contextRepository;
        this.kubernetesDeploymentService = kubernetesDeploymentService;
    }

    public List<DeploymentDTO> findAll() {

        return deploymentRepository
                .findAll()
                .stream()
                .map(deploymentConverter)
                .collect(Collectors.toList());
    }

    public DeploymentDTO findById(Long id) {

        return deploymentRepository
                .findById(id)
                .map(deploymentConverter)
                .orElseThrow(DeploymentNotFoundException::new);
    }

    @Transactional
    public DeploymentDTO create(DeploymentCreateDTO dto) {

        validateDeploymentByName(dto.getName());

        Context context = contextRepository.findById(dto.getContextName()).orElseThrow(ContextNotFoundException::new);

        K8sDeployment deployment = new K8sDeployment();
        deployment.setImage(dto.getImage());
        deployment.setName(dto.getName());
        deployment.setContext(context);
        return saveAndDeploy(deployment);
    }

    @Transactional
    public DeploymentDTO update(DeploymentUpdateDTO dto) {

        validateDeploymentByName(dto.getName());

        K8sDeployment deployment = findDeploymentExactly(dto.getId());

        deployment.setImage(dto.getImage());
        deployment.setName(dto.getName());

        return saveAndDeploy(deployment);
    }

    @Transactional
    public DeploymentDTO deploymentImageUpdate(DeploymentImageUpdateDTO dto) {

        K8sDeployment deployment = findDeploymentExactly(dto.getId());

        deployment.setImage(dto.getImage());
        return saveAndDeploy(deployment);
    }

    @Transactional
    public void deleteById(Long id){
        deploymentRepository.deleteById(id);
    }

    private DeploymentDTO saveAndDeploy(K8sDeployment deployment) {
        K8sDeployment persistedDeployment = deploymentRepository.save(deployment);

        kubernetesDeploymentService.createOrReplaceDeployment(persistedDeployment);

        return deploymentConverter.apply(persistedDeployment);
    }

    private K8sDeployment findDeploymentExactly(Long id) {
        return deploymentRepository.findById(id).orElseThrow(DeploymentNotFoundException::new);
    }

    private void validateDeploymentByName(String name) {
        Optional<K8sDeployment> deploymentFoundByName = deploymentRepository.findByName(name);
        deploymentFoundByName.ifPresent(deployment -> {
            throw new DeploymentAlreadyCreatedByNameException();
        });
    }

}
