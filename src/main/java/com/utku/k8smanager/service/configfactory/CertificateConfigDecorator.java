package com.utku.k8smanager.service.configfactory;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;
import io.fabric8.kubernetes.client.ConfigBuilder;
import org.springframework.stereotype.Component;

@Component
public class CertificateConfigDecorator extends KubernetesConfigDecorator {

    @Override
    public boolean isApplicableFor(Context context) {
        return ClusterConnectionType.CERTIFICATES.equals(context.getClusterConnectionType());
    }

    @Override
    public void populate(Context context, ConfigBuilder configBuilder) {
        configBuilder
                .withClientCertData(context.getCertData())
                .withClientKeyData(context.getClientKey());
    }
}
