package com.utku.k8smanager.service.configfactory;

import com.utku.k8smanager.domain.Context;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;

public abstract class KubernetesConfigDecorator {

    public abstract boolean isApplicableFor(Context context);

    abstract void populate(Context context, ConfigBuilder configBuilder);

    public Config build(Context context) {
        ConfigBuilder configBuilder = new ConfigBuilder()
                .withMasterUrl(context.getCluster());

        populate(context, configBuilder);

        return configBuilder.build();
    }
}
