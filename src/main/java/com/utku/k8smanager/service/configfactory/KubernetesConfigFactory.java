package com.utku.k8smanager.service.configfactory;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.service.configfactory.KubernetesConfigDecorator;
import io.fabric8.kubernetes.client.Config;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class KubernetesConfigFactory {

    private final List<KubernetesConfigDecorator> decorators;

    public KubernetesConfigFactory(List<KubernetesConfigDecorator> decorators) {
        this.decorators = decorators;
    }

    public Config buildFor(Context context) {
        for (KubernetesConfigDecorator decorator : decorators) {
            if (decorator.isApplicableFor(context)) {
                return decorator.build(context);
            }
        }

        throw new IllegalArgumentException();
    }

}
