package com.utku.k8smanager.service;

import com.utku.k8smanager.converter.ContextConverter;
import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.model.dto.ContextDTO;
import com.utku.k8smanager.repository.ContextRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ContextService {

    private final ContextRepository contextRepository;
    private final ContextConverter contextConverter;

    public ContextService(ContextRepository contextRepository, ContextConverter contextConverter) {
        this.contextRepository = contextRepository;
        this.contextConverter = contextConverter;
    }

    public List<ContextDTO> findAll() {
        return contextRepository
                .findAll()
                .stream()
                .map(contextConverter)
                .collect(Collectors.toList());
    }

    public ContextDTO findByName(String contextName) {
        Optional<Context> optionalContext = contextRepository.findById(contextName);

        return optionalContext.map(contextConverter).orElseThrow(ContextNotFoundException::new);
    }

}
