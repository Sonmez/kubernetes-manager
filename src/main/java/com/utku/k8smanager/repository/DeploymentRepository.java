package com.utku.k8smanager.repository;

import com.utku.k8smanager.domain.K8sDeployment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeploymentRepository extends JpaRepository<K8sDeployment, Long> {

    Optional<K8sDeployment> findByName(String name);

}
