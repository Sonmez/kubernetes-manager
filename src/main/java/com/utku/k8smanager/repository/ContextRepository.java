package com.utku.k8smanager.repository;

import com.utku.k8smanager.domain.Context;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContextRepository extends JpaRepository<Context, String> {

}
