package com.utku.k8smanager.security;

import java.util.Collections;
import java.util.List;

public class SecurityConstants {
    public static final String ADMIN_ROLE = "ADMIN";
    public static final List<String> ROLES = Collections.singletonList(ADMIN_ROLE);
}
