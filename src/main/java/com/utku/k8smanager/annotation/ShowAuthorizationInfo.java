package com.utku.k8smanager.annotation;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Request needs valid authorization token.", paramType = "header", dataTypeClass = String.class, required = true)
})
public @interface ShowAuthorizationInfo {

}