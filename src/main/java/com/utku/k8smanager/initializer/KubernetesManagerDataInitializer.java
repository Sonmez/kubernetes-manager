package com.utku.k8smanager.initializer;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.domain.User;
import com.utku.k8smanager.domain.enums.ClusterConnectionType;
import com.utku.k8smanager.repository.ContextRepository;
import com.utku.k8smanager.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class KubernetesManagerDataInitializer implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(KubernetesManagerDataInitializer.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ContextRepository contextRepository;

    public KubernetesManagerDataInitializer(UserRepository userRepository, PasswordEncoder passwordEncoder, ContextRepository contextRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.contextRepository = contextRepository;
    }

    @Override
    public void run(String... args) {
        LOGGER.debug("starting data initialization");

        User user1 = new User();
        user1.setEmail("admin@admin.com");
        user1.setPassword(passwordEncoder.encode("123456"));
        userRepository.save(user1);
        LOGGER.debug("user1 persisted");

        User user2 = new User();
        user2.setEmail("admin2@admin.com");
        user2.setPassword(passwordEncoder.encode("HASHED"));
        userRepository.save(user2);
        LOGGER.debug("user2 persisted");

        Context context = new Context();
        context.setClusterConnectionType(ClusterConnectionType.CERTIFICATES);
        context.setName("minikube");
        context.setCluster("https://192.168.64.2:8443");

        context.setCertData("-----BEGIN CERTIFICATE-----\n" +
                "MIIDcjCCAlqgAwIBAgIBAjANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDEwptaW5p\n" +
                "a3ViZUNBMB4XDTE5MDgxNTEwNTYxMFoXDTIwMDgxNDEwNTYxMFowLDEXMBUGA1UE\n" +
                "ChMOc3lzdGVtOm1hc3RlcnMxETAPBgNVBAMTCG1pbmlrdWJlMIIBIjANBgkqhkiG\n" +
                "9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFB9V1ORkJ5T7G+Oo73F45tfLd+UzdG1VNzV\n" +
                "E02O/82dvVEBlXZu6NTEY7zOJ0Da+rnu53iluC4Iz90c1NVuHnWUBnOsPzntpEju\n" +
                "cZPyg/O7ahlklIIRVIRT+RH5T8X7Tk2oB8wfh63uTHPOpdBqQNcoxf+7TwC5eQYH\n" +
                "AIVnRFmthHV0jFxg3pk/8AaFzBWHnAgF9+BiKzM/lBo84ZeYRBR0OIr8dAQo+q6d\n" +
                "rpT+scPVNda2K6XwzglwAcG+vRE8LhUKb1+NEuG1TzRvEBpBwTsXs9ckqxHLLhPF\n" +
                "kz0Kg2jC/+Lfb6HikwjOSm4TLCGTyax+kYaBnDxUJRkjropB7wIDAQABo4G1MIGy\n" +
                "MA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIw\n" +
                "DAYDVR0TAQH/BAIwADBzBgNVHREEbDBqgiRrdWJlcm5ldGVzLmRlZmF1bHQuc3Zj\n" +
                "LmNsdXN0ZXIubG9jYWyCFmt1YmVybmV0ZXMuZGVmYXVsdC5zdmOCEmt1YmVybmV0\n" +
                "ZXMuZGVmYXVsdIIKa3ViZXJuZXRlc4cEwKhAAocECgAAATANBgkqhkiG9w0BAQsF\n" +
                "AAOCAQEANS7Wp7TURqZehk5NX7yzn43a/KjjfPceMqldiQ8htdOdSYgd9uvE2OBN\n" +
                "6srni9DkfO4fkWWXmR6XDRVS9DAmL5Hvwo4FiWGCS637ZEWC+6l7DnrcUlUsjfON\n" +
                "9D6uJvWucsOSwgVDvIx8jjvg4sFCwGG++GgDKqDuC4Dj4m6dRkY2ilj5Ob2qCPbJ\n" +
                "ebLKJJG2g1S/Q5iGrte2faDv8W8F0qRDXA3P9KO1AhvR+0tyvPuIAcsfsp1l7BXy\n" +
                "UwMl6kbmYsp29Tb/nlEuFkrmMIW58lgmsC0GoO/DO80KjBN1nNWyZ9Wg9XjAtu3e\n" +
                "bVcpoy2NfKWPclPbDyH8Z+seikYMZg==\n" +
                "-----END CERTIFICATE-----\n");

        context.setClientKey("-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIEpAIBAAKCAQEAyFB9V1ORkJ5T7G+Oo73F45tfLd+UzdG1VNzVE02O/82dvVEB\n" +
                "lXZu6NTEY7zOJ0Da+rnu53iluC4Iz90c1NVuHnWUBnOsPzntpEjucZPyg/O7ahlk\n" +
                "lIIRVIRT+RH5T8X7Tk2oB8wfh63uTHPOpdBqQNcoxf+7TwC5eQYHAIVnRFmthHV0\n" +
                "jFxg3pk/8AaFzBWHnAgF9+BiKzM/lBo84ZeYRBR0OIr8dAQo+q6drpT+scPVNda2\n" +
                "K6XwzglwAcG+vRE8LhUKb1+NEuG1TzRvEBpBwTsXs9ckqxHLLhPFkz0Kg2jC/+Lf\n" +
                "b6HikwjOSm4TLCGTyax+kYaBnDxUJRkjropB7wIDAQABAoIBAE9cvFwVyu0+HUfm\n" +
                "Zt/utvFXRF1bYIwu0qXdnDm6l7Y3U5CHAJajxotOiMRvMEoTAu8lLUbd4OJYsrsQ\n" +
                "AnU8XhWJRe1MdOjT3ZgiIP40vHeU4CLIDrZb1dvjVZWvjlQRv2AlmjYEpFVQDsKR\n" +
                "e0TsjrxDwm63xBEn21PclHebYu+L2KcmkI7kK9D5eto3zS+oW/kx6zDtsFv2mza8\n" +
                "0SgN+r0mt41ibr8vJlbBltfZjL+Vp8QAI6RkCaGGOt1DVAkqPZ/TVMfyzVs7gLAt\n" +
                "dxUiM6qAIzp9fsWtk6CfTCNVWka8VKHoi7h6NjUKBthTd3nHquSxXagB5q90yiW6\n" +
                "Snr3CFECgYEA4PS8WWAf+3P9NRG36FM/3+hunlRLf0dbYVJUUC/xVFJs4UqCJHV6\n" +
                "YwcmcyVTr0BPt79bWNtg9OKvhDEDVUSWr5AiLKNVboOvTRsd3HqOUCKStiQTjAhE\n" +
                "JS8N4xKlpiwysacG6B2vesdxxst0KrUaswHDpOxEKBLXOVl53SzLq5kCgYEA4/U2\n" +
                "v2gYXOL4clscckX5azptjmDQkooDrAACre4bK7BApPs/DzX6j1dCvXJ3UwBIeUwC\n" +
                "J/+XywzsiW7lnrdtrMmi4XjKZ54L7S5Cfmfnkcd6Snv9H5cOqxdvh+hd6lRThitJ\n" +
                "hxcMHzeyPhB/jfZ89S+1AYDm6BcyyTuuGhjxjscCgYBds1Mo3zMzJ/MWQdUavmJi\n" +
                "v4wkAvSXhX94oDRiWjXpa5PSm5ht20kpKndHiwq6es05ukkwT+Co4Cs/orLCt5kU\n" +
                "c7LoaPhS2iMQuPTNO3c/xcbae1hxAZhw7JKaPNAm0U4kVXA5CvRm7eBkb6Zpi5od\n" +
                "aeS2M3uIPaDMUIrEJ76GMQKBgQCYJ7TDTdf1KAptSiNSoyI4UGeQAU82gKiyxc8E\n" +
                "CUpZfuunM4yDuqPjyXriuOSsoiO90t4dlJzdC2NW2I6rwgOSsYPmu5leUD/lsFrx\n" +
                "xKjsxRj7Xoc17r37tUjcjl0ha+fURlBtuxR8QdDXXGvHdh8yCQXoWwHFKLbdWth6\n" +
                "0utpQQKBgQCnLm7ko9wK3Vyhp0CnWHkhSiPA2PZJeNu9rg3hsD2XkdkDtwm7KDmg\n" +
                "TEgMDx4Z0Q/AfGolz2ZYi6flwe6UmzdZuKEUiXRWJ4VaXgGSEswE9hullDc2UZOT\n" +
                "X0sbBmmx4ycMoPdyGj2DzmxNFFv1l8Xhqr0Vjrp24zaBurSQzICMQQ==\n" +
                "-----END RSA PRIVATE KEY-----\n");
        contextRepository.save(context);
        LOGGER.debug("minikube context persisted");

        LOGGER.debug("completing data initialization");
    }
}
