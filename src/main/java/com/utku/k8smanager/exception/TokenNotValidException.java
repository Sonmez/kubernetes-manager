package com.utku.k8smanager.exception;

import org.springframework.security.core.AuthenticationException;

public class TokenNotValidException extends AuthenticationException {

    public TokenNotValidException(String message) {
        super(message);
    }

}
