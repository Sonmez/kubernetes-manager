package com.utku.k8smanager.exception;

import org.springframework.http.HttpStatus;

public class K8sManagerException extends RuntimeException {

    private final HttpStatus httpStatus;

    public K8sManagerException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
