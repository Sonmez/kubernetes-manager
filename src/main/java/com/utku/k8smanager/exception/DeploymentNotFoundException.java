package com.utku.k8smanager.exception;

import org.springframework.http.HttpStatus;

public class DeploymentNotFoundException extends K8sManagerException {

    public DeploymentNotFoundException() {
        super("deployment couldn't be found", HttpStatus.NOT_FOUND);
    }
}
