package com.utku.k8smanager.exception;

import org.springframework.http.HttpStatus;

public class DeploymentAlreadyCreatedByNameException extends K8sManagerException {

    public DeploymentAlreadyCreatedByNameException() {
        super("Deployment already created by name", HttpStatus.BAD_REQUEST);
    }
}
