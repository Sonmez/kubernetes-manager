package com.utku.k8smanager.exception;

import org.springframework.http.HttpStatus;

public class ContextNotFoundException extends K8sManagerException {

    public ContextNotFoundException() {
        super("context couldn't be found", HttpStatus.NOT_FOUND);
    }
}
