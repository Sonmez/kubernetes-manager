package com.utku.k8smanager.converter;

import com.utku.k8smanager.domain.Context;
import com.utku.k8smanager.model.dto.ContextDTO;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ContextConverter implements Function<Context, ContextDTO> {

    @Override
    public ContextDTO apply(Context context) {
        ContextDTO dto = new ContextDTO();
        dto.setCluster(context.getCluster());
        dto.setClusterConnectionType(context.getClusterConnectionType());
        dto.setName(context.getName());
        return dto;
    }

}
