package com.utku.k8smanager.converter;

import com.utku.k8smanager.domain.K8sDeployment;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class DeploymentConverter implements Function<K8sDeployment, DeploymentDTO> {

    @Override
    public DeploymentDTO apply(K8sDeployment deployment) {
        DeploymentDTO dto = new DeploymentDTO();
        dto.setId(deployment.getId());
        dto.setImage(deployment.getImage());
        dto.setName(deployment.getName());
        dto.setContextName(deployment.getContext().getName());
        return dto;
    }

}
