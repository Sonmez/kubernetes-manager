package com.utku.k8smanager.controller;

import com.utku.k8smanager.annotation.ShowAuthorizationInfo;
import com.utku.k8smanager.model.request.LoginRequest;
import com.utku.k8smanager.model.response.LoginResponse;
import com.utku.k8smanager.service.AuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public LoginResponse createAuthenticationToken(@RequestBody LoginRequest loginRequest) {
        String token = authenticationService.authenticate(loginRequest.getEmail(), loginRequest.getPassword());
        return new LoginResponse("Bearer " + token);
    }

    @PostMapping("/valid")
    @ResponseStatus(HttpStatus.OK)
    @ShowAuthorizationInfo
    public void checkToken() {
        // do nothing
    }

}
