package com.utku.k8smanager.controller;

import com.utku.k8smanager.annotation.ShowAuthorizationInfo;
import com.utku.k8smanager.model.dto.DeploymentCreateDTO;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import com.utku.k8smanager.model.dto.DeploymentImageUpdateDTO;
import com.utku.k8smanager.model.dto.DeploymentUpdateDTO;
import com.utku.k8smanager.model.resource.DeploymentResource;
import com.utku.k8smanager.service.DeploymentService;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/contexts/{context-name}/deployments")
public class DeploymentController {

    private final DeploymentService deploymentService;

    public DeploymentController(DeploymentService deploymentService) {
        this.deploymentService = deploymentService;
    }

    @GetMapping
    public Resources<DeploymentResource> all(@PathVariable("context-name") String contextName) {
        List<DeploymentDTO> deployments = deploymentService.findAll();
        List<DeploymentResource> deploymentResources = deployments.stream().map(DeploymentResource::new).collect(Collectors.toList());
        return new Resources<>(deploymentResources);
    }

    @GetMapping("/{id}")
    public DeploymentResource get(@PathVariable("context-name") String contextName, @PathVariable("id") Long id) {
        DeploymentDTO deploymentDTO = deploymentService.findById(id);
        return new DeploymentResource(deploymentDTO);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ShowAuthorizationInfo
    public DeploymentResource post(@PathVariable("context-name") String contextName, @RequestBody DeploymentCreateDTO dto) {
        dto.setContextName(contextName);
        DeploymentDTO deploymentDTO = deploymentService.create(dto);
        return new DeploymentResource(deploymentDTO);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ShowAuthorizationInfo
    public DeploymentResource post(@PathVariable("context-name") String contextName,
                                   @PathVariable("id") Long id,
                                   @RequestBody DeploymentUpdateDTO dto) {
        dto.setId(id);
        DeploymentDTO deploymentDTO = deploymentService.update(dto);
        return new DeploymentResource(deploymentDTO);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ShowAuthorizationInfo
    public DeploymentResource post(@PathVariable("context-name") String contextName,
                                   @PathVariable("id") Long id,
                                   @RequestBody DeploymentImageUpdateDTO dto) {
        dto.setId(id);
        DeploymentDTO deploymentDTO = deploymentService.deploymentImageUpdate(dto);
        return new DeploymentResource(deploymentDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ShowAuthorizationInfo
    public void post(@PathVariable("context-name") String contextName,
                                   @PathVariable("id") Long id) {
        deploymentService.deleteById(id);
    }

}
