package com.utku.k8smanager.controller;

import com.utku.k8smanager.annotation.ShowAuthorizationInfo;
import com.utku.k8smanager.model.dto.ContextDTO;
import com.utku.k8smanager.model.resource.ContextResource;
import com.utku.k8smanager.service.ContextService;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/contexts")
public class ContextController {

    private final ContextService contextService;

    public ContextController(ContextService contextService) {
        this.contextService = contextService;
    }

    @GetMapping
    public Resources<ContextResource> all() {
        List<ContextDTO> contexts = contextService.findAll();
        List<ContextResource> contextResources = contexts.stream().map(ContextResource::new).collect(Collectors.toList());
        return new Resources<>(contextResources);
    }

    @GetMapping("/{name}")
    public ContextResource get(@PathVariable("name") String name) {
        ContextDTO contextDTO = contextService.findByName(name);
        return new ContextResource(contextDTO);
    }

}
