package com.utku.k8smanager.model.dto;

import com.utku.k8smanager.domain.enums.ClusterConnectionType;

public class ContextDTO {

    private String name;
    private String cluster;
    private ClusterConnectionType clusterConnectionType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public ClusterConnectionType getClusterConnectionType() {
        return clusterConnectionType;
    }

    public void setClusterConnectionType(ClusterConnectionType clusterConnectionType) {
        this.clusterConnectionType = clusterConnectionType;
    }
}
