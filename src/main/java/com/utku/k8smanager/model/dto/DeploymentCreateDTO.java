package com.utku.k8smanager.model.dto;

import javax.validation.constraints.NotNull;

public class DeploymentCreateDTO {
    @NotNull
    private String name;

    @NotNull
    private String image;

    private String contextName;

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
