package com.utku.k8smanager.model.dto;

import javax.validation.constraints.NotNull;

public class DeploymentImageUpdateDTO {

    @NotNull
    private String image;

    private Long id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
