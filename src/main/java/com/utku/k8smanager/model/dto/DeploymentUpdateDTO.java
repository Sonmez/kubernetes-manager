package com.utku.k8smanager.model.dto;

import javax.validation.constraints.NotNull;

public class DeploymentUpdateDTO {

    @NotNull
    private String name;

    @NotNull
    private String image;

    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
