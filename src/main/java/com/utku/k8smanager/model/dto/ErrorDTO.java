package com.utku.k8smanager.model.dto;

public class ErrorDTO {

    private String title;
    private int status;
    private String detail;
    private String requestUri;
    private String requestMethod;
    private String instant;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getInstant() {
        return instant;
    }

    public void setInstant(String instant) {
        this.instant = instant;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final ErrorDTO errorDTO;

        private Builder() {
            errorDTO = new ErrorDTO();
        }

        public Builder title(String title) {
            errorDTO.setTitle(title);
            return this;
        }

        public Builder status(int status) {
            errorDTO.setStatus(status);
            return this;
        }

        public Builder detail(String detail) {
            errorDTO.setDetail(detail);
            return this;
        }

        public Builder requestUri(String requestUri) {
            errorDTO.setRequestUri(requestUri);
            return this;
        }

        public Builder requestMethod(String requestMethod) {
            errorDTO.setRequestMethod(requestMethod);
            return this;
        }

        public Builder instant(String instant) {
            errorDTO.setInstant(instant);
            return this;
        }

        public ErrorDTO build() {
            return errorDTO;
        }
    }
}