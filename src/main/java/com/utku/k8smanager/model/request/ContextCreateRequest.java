package com.utku.k8smanager.model.request;

import com.utku.k8smanager.domain.enums.ClusterConnectionType;

public class ContextCreateRequest {

    private String contextName;
    private String username;
    private String password;
    private String cluster;
    private String certData;
    private String clientKey;
    private ClusterConnectionType clusterConnectionType;

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCertData() {
        return certData;
    }

    public void setCertData(String certData) {
        this.certData = certData;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public ClusterConnectionType getClusterConnectionType() {
        return clusterConnectionType;
    }

    public void setClusterConnectionType(ClusterConnectionType clusterConnectionType) {
        this.clusterConnectionType = clusterConnectionType;
    }
}
