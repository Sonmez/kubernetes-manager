package com.utku.k8smanager.model.resource;

import com.utku.k8smanager.controller.DeploymentController;
import com.utku.k8smanager.model.dto.DeploymentDTO;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class DeploymentResource extends ResourceSupport {

    private final DeploymentDTO deployment;

    public DeploymentResource(DeploymentDTO deploymentDTO) {
        this.deployment = deploymentDTO;
        add(linkTo(methodOn(DeploymentController.class).all(deploymentDTO.getContextName())).withRel("deployments"));
        add(linkTo(methodOn(DeploymentController.class).get(deploymentDTO.getContextName(), deploymentDTO.getId())).withSelfRel());
    }

    public DeploymentDTO getDeployment() {
        return deployment;
    }
}
