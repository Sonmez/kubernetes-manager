package com.utku.k8smanager.model.resource;


import com.utku.k8smanager.controller.ContextController;
import com.utku.k8smanager.controller.DeploymentController;
import com.utku.k8smanager.model.dto.ContextDTO;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class ContextResource extends ResourceSupport {

    private final ContextDTO context;

    public ContextResource(final ContextDTO context) {
        this.context = context;

        add(linkTo(ContextController.class).withRel("contexts"));
        add(linkTo(methodOn(DeploymentController.class).all(context.getName())).withRel("deployments"));
        add(linkTo(methodOn(ContextController.class).get(context.getName())).withSelfRel());
    }

    public ContextDTO getContext() {
        return context;
    }
}
