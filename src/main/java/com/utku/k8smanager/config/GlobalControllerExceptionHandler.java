package com.utku.k8smanager.config;

import com.utku.k8smanager.exception.ContextNotFoundException;
import com.utku.k8smanager.exception.DeploymentNotFoundException;
import com.utku.k8smanager.exception.K8sManagerException;
import com.utku.k8smanager.exception.TokenNotValidException;
import com.utku.k8smanager.model.dto.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalControllerExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
    private static final String EXCEPTION_OCCURRED_MESSAGE = "Exception occurred";

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDTO> handleException(HttpServletRequest request, Exception exception) {
        LOGGER.error("Exception: ", exception);
        ErrorDTO errorDTO = buildErrorDTO(request, HttpStatus.INTERNAL_SERVER_ERROR, EXCEPTION_OCCURRED_MESSAGE);
        return new ResponseEntity<>(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(K8sManagerException.class)
    public ResponseEntity<ErrorDTO> handle(HttpServletRequest request, K8sManagerException exception) {
        LOGGER.info("Exception: ", exception);
        ErrorDTO errorDTO = buildErrorDTO(request, exception.getHttpStatus(), exception.getMessage());
        return new ResponseEntity<>(errorDTO, exception.getHttpStatus());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorDTO> handle(HttpServletRequest request, BadCredentialsException exception) {
        LOGGER.error("Exception: ", exception);
        ErrorDTO errorDTO = buildErrorDTO(request, HttpStatus.FORBIDDEN, exception.getMessage());
        return new ResponseEntity<>(errorDTO, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(TokenNotValidException.class)
    public ResponseEntity<ErrorDTO> handle(HttpServletRequest request, TokenNotValidException exception) {
        LOGGER.error("Exception: ", exception);
        ErrorDTO errorDTO = buildErrorDTO(request, HttpStatus.FORBIDDEN, exception.getMessage());
        return new ResponseEntity<>(errorDTO, HttpStatus.FORBIDDEN);
    }

    private ErrorDTO buildErrorDTO(HttpServletRequest request, HttpStatus statusType, String exceptionDetailMessage) {
        return ErrorDTO.builder()
                .title(statusType.getReasonPhrase())
                .status(statusType.value())
                .detail(exceptionDetailMessage)
                .requestUri(request.getRequestURI())
                .requestMethod(request.getMethod())
                .instant(Instant.now().atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT))
                .build();
    }

}